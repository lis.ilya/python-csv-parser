import csv, datetime, re, idna, xlsxwriter, pdb, phonenumbers
workbook = xlsxwriter.Workbook('SMS_'+datetime.datetime.now().strftime('%y%m%d')+'.xlsx')
worksheet = workbook.add_worksheet()
worksheet.write('A1', 'Name')
worksheet.write('B1', 'Domain')
worksheet.write('C1', 'Renew')
worksheet.write('D1', 'Expire')
worksheet.write('E1', 'Renewal')
worksheet.write('F1', 'Phone')
worksheet.write('G1', 'Email')
col=0
r=1
op = ['lifecell', 'Kyivstar', 'Vodafone']
from phonenumbers import (carrier, region_code_for_country_code, region_code_for_number)
from datetime import timedelta
with open('domains.csv', 'r', encoding='utf-8') as inp:
    inp = csv.reader(inp)
    inp.__next__()
    for row in inp:
        n = row[6]
        i = row[7]
        d = idna.decode(row[2])
        renew = row[10] if row[11] == '--' else datetime.datetime.strptime(row[11], '%d %b %Y %H:%M %Z').strftime('%d.%m.%Y')
        e = datetime.datetime.strptime(row[4], '%d %b %Y %H:%M %Z').strftime('%d.%m.%Y')
        rd = datetime.datetime.strptime(row[4], '%d %b %Y %H:%M %Z') - datetime.timedelta(days=2)
        # p = re.sub("^\+?3?8?(0[5-9][0-9]\d{7})$", "", row[22])
        if row[23] != '':
        	p = re.sub("^\+?3?8?(0[5-9][0-9]\d{7})$", "", row[23])
        else:   
            p = re.sub("^\+?3?8?(0[5-9][0-9]\d{7})$", "", row[33])
        try:
            num = phonenumbers.parse(p, 'UA')
        except phonenumbers.phonenumberutil.NumberParseException:
            continue
        if phonenumbers.is_valid_number(num):
            c = carrier.name_for_number(num, "UA")
            if c in op and (region_code_for_country_code(num.country_code) == "UA"):
                p = phonenumbers.format_number(num, phonenumbers.PhoneNumberFormat.E164)
            else:
                continue
        else:
            continue
        worksheet.write(r, col, n)
        worksheet.write(r, col + 1, d)
        worksheet.write(r, col + 2, rd.strftime('%d.%m.%Y'))
        worksheet.write(r, col + 3, e)
        worksheet.write(r, col + 4, renew)
        worksheet.write(r, col + 5, p)
        worksheet.write(r, col + 6, i)
        r += 1
workbook.close()